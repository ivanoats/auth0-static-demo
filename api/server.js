const http = require('http');
const cors = require('cors');
const express = require('express');
const jwt = require('express-jwt');
const quoter = require('./quoter');

var app = express();

app.options('*', cors());
app.use(cors());

if (process.env.NODE_ENV === 'development') {
  const logger = require('morgan');
  const errorhandler = require('errorhandler');

  app.use(logger('dev'));
  app.use(errorhandler());
}

app.get('/', (req, res, next) => {
  res.end('Quotes API');
});

var jwtCheck = jwt({
  secret: new Buffer(process.env.AUTH0_CLIENT_SECRET, 'base64'),
  audience: process.env.AUTH0_CLIENT_ID
});

app.get('/random-quote', jwtCheck, function(req, res) {
  res.status(200).send(quoter.getRandomOne());
});

var port = process.env.PORT || 3001;

http.createServer(app).listen(port, function (err) {
  console.log('Quotes API server listening at http://localhost:' + port);
});
