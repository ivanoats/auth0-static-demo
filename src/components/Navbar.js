import React, { Component, PropTypes } from 'react'
import Login from './Login'
import Logout from './Logout'
import { login, logoutUser } from '../actions'

export default class Navbar extends Component {

  render() {
    const { dispatch, isAuthenticated, errorMessage, profile } = this.props

    return (
      <nav className='navbar navbar-default'>
        <div className='container-fluid'>
          <a className="navbar-brand" href="#">Quotes App</a>
           <div className='navbar-form'>

           {!isAuthenticated &&
             <Login
               errorMessage={errorMessage}
               onLoginClick={ () => dispatch(login()) }
             />
           }

           {isAuthenticated &&
             <div>
               { profile && <span>Welcome {profile.nickname}</span> }
               <Logout onLogoutClick={() => dispatch(logoutUser())} />
             </div>
           }

         </div>
       </div>
     </nav>
   );
  }

}

Navbar.propTypes = {
  dispatch: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string
}
